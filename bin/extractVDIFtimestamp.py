#!/usr/bin/env python
"""
extractVDIFtimestamp.py - Use baseband to extract session
    info from the headers

User extractVDIFtimestamp.py -h to see the help menu
@author: gofrito
v0.1 Works with vdif data
"""
from argparse import ArgumentParser, ArgumentTypeError
from baseband import vdif

# declare command line arguments
parser = ArgumentParser(
    prog='ExtractTimeStamps',
    description='Extract the start and stop times from the raw VDIF file')
parser.add_argument('infile_path',
    help='Path to input VDIF file.')

args = parser.parse_args()

# Open the VDIF data file
header_info = vdif.open(args.infile_path)

# Take the number of channels from sampleshape class
h_shape = header_info.sample_shape
if hasattr(h_shape, 'nchan'):
    num_channels = h_shape.nchan
else:
    num_channels = 1

print('***************************************************')
print(f'sample_rate       : {header_info.sample_rate}')
print(f'samples_per_frame : {header_info.samples_per_frame}')
print(f'bits per sample   : {header_info.bps}')
print(f'Number of channels: {num_channels}')
print(f'start_time        : {header_info.start_time}')
print(f'stop_time         : {header_info.stop_time}')
print('***************************************************')

header_info.close()
