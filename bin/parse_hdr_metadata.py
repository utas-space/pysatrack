#!/usr/bin/env python
import argparse
import os
import re
import pmt
import datetime as dt

from gnuradio.blocks import parse_file_metadata

parser = argparse.ArgumentParser()
parser.add_argument("filename", nargs="?", help="spectra input file", default="check_string_for_empty")
args = parser.parse_args()

handle = open(args.filename, 'rb')

header_str = handle.read(parse_file_metadata.HEADER_LENGTH)

if(len(header_str) == 0):
    print('error')

try:
    header = pmt.deserialize_str(header_str)
except RuntimeError:
    print('error')

info = parse_file_metadata.parse_header(header, False)
if(info["extra_len"] > 0):
    extra_str = handle.read(info["extra_len"])
    if(len(extra_str) != 0):
        extra = pmt.deserialize_str(extra_str)
        extra_info = parse_file_metadata.parse_extra_dict(extra, info, True)

datafile = args.filename.replace('.hdr', '')

filesize = os.path.getsize(datafile)
file_duration = filesize/(2 * 4 * int(info['rx_rate']))

print(f"Sample rate   : {info['rx_rate']} Msps")
print(f"Center Freq   : {info['rx_freq']} Hz")
print(f"Data type     : {info['type']}")
print(f"Data size     : {info['size']}" )
print(f"Start time    : {dt.datetime.fromtimestamp(info['rx_time'], dt.timezone.utc)}")
print(f"Duration      : {file_duration}")