#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 10:49:40 2020

v0.2 displaySpectra.py - fixed timestamps
v0.1 displaySpectra.py - an updated version of calculateCpp
@author: gofrito
"""
import argparse
import errno
import os
import re
import time
import configparser

import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go

from skyfield.api import load, wgs84
from spacetrack import SpaceTrackClient
from pysctrack import core

# Add logo on the right corner
from PIL import Image

PYSATTRACK = os.path.abspath(os.path.dirname(__file__))[:-3]

# Use configparser package to pull in the ini file (pip install configparser)
config = configparser.ConfigParser()
config.read(f"{PYSATTRACK}SLTrack.ini")
configUsr = config.get("configuration","username")
configPwd = config.get("configuration","password")
configOut = config.get("configuration","output")
siteCred = {'identity': configUsr, 'password': configPwd}

# Read the figure with PIL
pyLogo = Image.open(f"{PYSATTRACK}data/watermark.png")

global output
output: bool = False

class plotSpectra:
    def __init__(self):
        global output

        # List of parsed parameters
        parser = argparse.ArgumentParser()
        parser.add_argument("-bw", dest="bandwidth", help="observing bandwidth", type=float, default=32)
        parser.add_argument("-zf", dest="zoom_frequency", help="search window boundaries in [MHz]", nargs="+", type=float, metavar="N", default=(4.7, 4.9))
        parser.add_argument("filename", nargs="?", help="spectra input file", default="check_string_for_empty")
        parser.add_argument("-t0", dest="ini_time", help="initial time within the scan", type=int, default=0)
        parser.add_argument("-t1", dest="end_time", help="end time within the scan", type=int, default=0)
        parser.add_argument("-t2", dest="select_time", help="select spectrum", type=int, default=0)
        parser.add_argument("-rf", dest="radio_frequency", help="Sky freq of the channel", type=float, default=0)
        parser.add_argument("-s", dest="saveplot", help="Store the plot in a file", type=int, default=0)
        parser.add_argument("-addlogo", help="Add logo to the saved fil", action="store_true", default=False)
        parser.add_argument("-norad", dest="norad", help='add NORAD code', type=int, default=0)
        parser.add_argument("-mask", dest="mask", help="mask a range of frequeencies", type=bool, default=False)
        args = parser.parse_args()

        self.BW: float = args.bandwidth
        self.Fmin, self.Fmax = args.zoom_frequency

        # Set observation-dependant parameters
        self.t0: float = args.ini_time
        self.t1: float = args.end_time
        self.t2: float = args.select_time

        if self.t2 == 0:
            self.t2 == self.t1

        self.file_plot: int = args.saveplot

        if self.file_plot > 0:
            output = True

        self.logo: bool = args.addlogo

        # Check if the file exists
        if os.path.exists(args.filename):
            path, self.filename = os.path.split(args.filename)
            self.path: str = f"{os.path.abspath(path)}/"
        else:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.filename)

        # Convert to MHz
        self.BW *= 1e6
        self.Fmin *= 1e6
        self.Fmax *= 1e6

        self.norad = int(args.norad)

        # Unless we force the settings manually
        if args.radio_frequency:
            self.Fstart: float = args.radio_frequency

        """
        Filename format : Session name + Station + Data Format + scan number + FFT points + integration time + channel + _swpec.bin
        Example: m200101_Mh_VDIF_No0001_3200000pt_5s_ch1_swspec.bin
        Split the file in 7 fields:
        """
        match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:N)?([oabcdefgh])?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_([^_]*)", self.filename, re.I)

        # Extract all observing params from the filename
        self.session: str = match.group(1)
        self.station: str = match.group(2)
        self.satellite: str = match.group(3)
        self.ifband: str = match.group(4)
        self.scan_number: int = int(match.group(5))
        self.FFTpt: int = int(match.group(6))
        self.dts: int = int(match.group(7))
        self.channel: int = int(match.group(8))
        software: str = match.group(9)[0:6]

        # if t1 = 0 use the entire file, otherwise use t1
        if self.t1:
            self.Nspec = int(self.t1 / self.dts)
        else:
            fsize: int = os.path.getsize(f"{self.path}{self.filename}")
            self.Nspec: int = int(np.floor(0.5 * fsize / (self.FFTpt + 1)))
            self.t1: float = self.Nspec * self.dts

        # Reading starting time of the scan
        basefilen: str = np.size(self.filename) - 12
        timebin: str = f"{self.path}{self.filename[0:basefilen]}_starttiming.txt"
        self.Start: float = 0
        if os.path.exists(timebin):
            Tsinfo = np.loadtxt(timebin, skiprows=1)
            self.Start = Tsinfo[1]

    def data_process(self):
        SR: int = 2 * self.BW
        dtsam: float = 1 / SR
        self.df: float = SR / self.FFTpt
        Nfft: int = int(self.FFTpt / 2 + 1)
        jf: int = np.arange(Nfft)
        self.ff = self.df * jf
 
      # Select the initial and end spectra
        s0: int = round(self.t0 / self.dts)
        s1: int = self.Nspec
        tsp: float = self.dts * (0.5 + np.arange(s0, s1))
        self.tsp: float = tsp + self.t0

        # Set the search window
        self.bfs: int = int(self.Fmin / self.df)
        self.bfm: int = int(self.Fmax / self.df)
        self.ffs = self.ff[self.bfs : self.bfm]

        self.Nspek: int(s1 - s0)
        self.zSpec: float = np.zeros((self.Nspek, self.bfm - self.bfs))
        self.aSpec: float = np.zeros(Nfft)

        # Read file
        fd = open(f"{self.path}{self.filename}", "rb")

        for ip in range(self.Nspec):
            one_spec: float = np.fromfile(file=fd, dtype="float32", count=Nfft)

            # Only store those spectrum in the selected time range
            if (ip >= s0) and (ip <= s1):
                # Use the spectrum for the stacked spectrum    
                self.aSpec += one_spec
                # Extract the zoom band and store in the array
                self.zSpec[ip,:] = one_spec[self.bfs : self.bfm]
                # Save spectrum t2 or the last if not selected
                if(int(self.t2 / self.dts) == ip):
                    self.lSpec = one_spec / one_spec.mean()
        
        self.Smax: float = np.zeros(self.Nspek)
        self.Fdets: float = np.zeros(self.Nspek)
        self.SNR: float = np.zeros(self.Nspek)
        self.SNR2: float = np.zeros(self.Nspek)

        for ip in range(self.Nspek):
            max_spec = np.max(self.zSpec[ip, :])
            max_bin = np.argmax(self.zSpec[ip, :])
            
            self.Smax[ip] = max_spec
            self.Fdets[ip] = self.df * max_bin + self.ffs[0]
            
            RMS = (self.zSpec[ip, 0:int(max_bin/2)].std() + self.zSpec[ip, int(max_bin/2):].std()) / 2
            mean_spec = (self.zSpec[ip, 0 : int(max_bin / 2)].mean() + self.zSpec[ip, int(max_bin / 2) : ].mean()) / 2

            self.SNR[ip] =  (max_spec - RMS) / mean_spec

            xfc = core.FindMax(self.zSpec[ip, :], self.ffs)
            RMS = core.GetRMS(self.zSpec[ip, :], xfc[1], 2000, 200)

            self.SNR2[ip] = (xfc[2] - RMS[0]) / RMS[1]

        # close the opened file
        fd.close()

        #self.lSpec = one_spec / one_spec.mean()
        self.aSpec /= self.aSpec.mean()
        self.zSpec /= self.zSpec.max()

        self.epoch = f'20{self.session[1:3]}.{self.session[3:5]}.{self.session[5:7]}'

        if (2000 < self.Fstart < 3000):
            self.band = 'S-band'
        elif (3000 < self.Fstart < 7000):
            self.band = 'C-band'
        elif (7000 < self.Fstart < 9000):
            self.band = 'X-band'
        elif(9000 < self.Fstart < 14000):
            self.band = 'Ku-band'
        
        if self.norad > 0:
            self.load_tle()

    def load_tle(self):
        ts = load.timescale()

        tle_file: str = f'{PYSATTRACK}data/3le.tle'

        satellites = load.tle_file(tle_file)

        sat_by_name = {sat.name: sat for sat in satellites}
        sat_by_number = {sat.model.satnum: sat for sat in satellites}

        satellite = sat_by_number[self.norad]

        hobart = wgs84.latlon(latitude_degrees = -42.8055808638581, longitude_degrees = 147.43813722942386, elevation_m = 40.983)
        katherine = wgs84.latlon(latitude_degrees=-14.3766210062618, longitude_degrees=132.15431778026138, elevation_m=188.978)
        yarragadee = wgs84.latlon(latitude_degrees=-29.04713995970614, longitude_degrees=115.3456309353691, elevation_m=248.23692528494942)
        ceduna = wgs84.latlon(latitude_degrees=-31.86767880595407, longitude_degrees=133.80983868862833, elevation_m=164.6178250238288)

        if self.station == 'Hb':
            station = hobart
        elif self.station == 'Ke':
            station = katherine
        elif self.station == 'Yg':
            station = yarragadee
        elif self.station == 'Cd':
            station = ceduna

        year = int(f'20{self.session[1:3]}')
        hour = self.Start%24
        minu = (self.Start - hour * 60)%60
        seco = self.Start - hour * 60 - minu * 60
        self.range_sc = range(0, self.Nspec * self.dts)

        t = ts.utc(year, int(self.session[3:5]), int(self.session[5:7]), hour, minu, seco + self.range_sc)

        position = (satellite - station).at(t)

        _, _, the_range, _, _, range_rate = position.frame_latlon_and_rates(station)

        frequency_hz = self.Fstart * 1e6
        self.doppler_shift = - range_rate.km_per_s * 1e3 / 299792458. * frequency_hz

    def create_plots(self):
        print("Checking the output spectra from swspectrometer")
        print("Select the figure that you would like to display")
        print("1- Power spectrum - single full bandwdith")
        print("2- Frequency detections of the satellite signal")
        print("3- Waterfall - dynamic spectrum")
        print("4- Topocentric detections and predictions")

        graph = input("Make a choice: 1-2-3-4\n")

        if graph == "1":
            plt.semilogy(self.ff, self.lSpec, color="deepskyblue", label="spectral power")
            plt.ylabel("Spectral power")
            plt.xlabel("Frequency [MHz]")
            plt.grid('on')
            plt.title(f"Last spectrum from the session {self.session} at {self.station}")
            plt.show()

        elif graph == "2":
            plt.plot(self.tsp, self.Fdets, color="mediumseagreen", marker="o", linestyle="none")
            plt.xlabel('Time [s]')
            plt.ylabel('Frequency [MHz]')
            plt.title(f'Topocentric frequency detections {self.session} at {self.station}')
            plt.show()

        elif graph == "3":
            dynspec = plt.pcolormesh(self.tsp, self.ffs, np.transpose(10 * np.log10(np.abs(self.zSpec))), cmap='magma', shading='auto')
            plt.colorbar(dynspec)
            plt.ylabel('Frequency [MHz]')
            plt.xlabel('Time [s]')
            plt.title(f'Dynamic spectrum {self.session} at {self.station} in dB')
            plt.show()

        elif graph == "4":
            plt.plot(self.tsp, self.Fdets, color="red", marker="o", linestyle="none")
            plt.plot(self.range_sc + self.tsp[0], self.doppler_shift, color="mediumseagreen", marker="o", linestyle="none")
            plt.ylabel('Frequency [MHz]')
            plt.xlabel('Time [s]')
            plt.show()

        elif graph == "5":
            plt.plot(self.tsp, 10 * np.log10(self.SNR), color="red", marker="o", linestyle="none")
            plt.plot(self.tsp, 10 * np.log10(self.SNR2), color="blue", marker="o", linestyle="none")
            plt.ylabel('Spectral max (dB)')
            plt.xlabel('Time [s]')
            plt.show()

    def save_plots(self):
        fig = go.Figure()

        if self.logo:
            # Add a logo
            fig.add_layout_image(
                dict(
                    source=pyLogo,
                    xref="paper", yref="paper",
                    x=1, y=1.05,
                    sizex=0.2, sizey=0.2,
                    xanchor="right", yanchor="bottom"
                )
            )  

        if self.file_plot == 1:
            self.ff /= 1e6
            self.ff += self.Fstart
            fig.add_traces(go.Scatter(mode='lines', x=self.ff, y=10 * np.log10(self.lSpec)))
            #fig.add_traces(go.Scatter(mode='lines', x=self.ff, y=10 * np.log10(self.aSpec)))

            # Add figure title
            fig.update_layout(
                title_text=f"Normalised power spectra from {self.satellite} on {self.epoch} at {self.station}<br><sup>The satellite was observed at {self.band}</sup>",
                font_family="lato",
                font_color="grey",
                title_font_size=18,
                title_font_color="blue",
                font_size=15,
                width=1200,
                height=600,
            )

            # Set y-axes titles
            fig.update_xaxes(title_text="<b>Frequency band [MHz] </b>")
            fig.update_yaxes(title='<b>Spectral power [dB]</b>')

            # Save to disk the image
            fig.write_image(f'{self.session}_{self.station}_{self.satellite}_Spectra.pdf')
 
        elif self.file_plot == 2:
            self.ff /= 1e6
            self.ff += self.Fstart
            fig.add_traces(go.Scatter(mode='lines', x=self.tsp, y=self.Fdets))

            # Add figure title
            fig.update_layout(
                title_text=f"Topocentric frequency detections from {self.satellite} on {self.epoch} at {self.station}<br><sup>Using an integration time of {self.dts} sec and frequency resolution of {self.df} Hz",
                font_family="lato",
                font_color="grey",
                title_font_size=18,
                title_font_color="blue",
                font_size=14,
                width=1200,
                height=600,
            )

            # Set y-axes titles
            fig.update_xaxes(title_text="<b>Time [s] </b>")
            fig.update_yaxes(title='<b>Frequency [MHz]</b>')

            # Save to disk the image
            fig.write_image(f'{self.session}_{self.station}_{self.satellite}_Fdets.pdf')
 
        elif self.file_plot == 3:
            self.ffs /= 1e6
            self.ffs += self.Fstart
            fig.add_traces(go.Heatmap(x = self.ffs, y = self.tsp, z = 10 * np.log10(np.abs(self.zSpec)), colorscale = 'Plasma'))

            # Add figure title
            fig.update_layout(
                title_text=f"Spectral waterfall of the carrier signal from {self.satellite} on {self.epoch} at {self.station}",
                font_family="lato",
                font_color="grey",
                title_font_size=18,
                title_font_color="blue",
                font_size=14,
                width=1200,
                height=900,
            )

            # Set y-axes titles
            fig.update_xaxes(title_text="<b>Frequency band [MHz]</b>")
            fig.update_yaxes(title='<b>Time [s]</b>')

            # Save to disk the image
            fig.write_image(f'{self.session}_{self.station}_{self.satellite}_Waterfall.pdf')

        elif self.file_plot == 4:
            self.ff /= 1e6
            self.ff += self.Fstart
            fig.add_traces(go.Scatter(mode='lines', x=self.tsp, y=self.Fdets))
            fig.add_traces(go.Scatter(mode='lines', x=self.tsp, y=self.doppler_shift))

            # Add figure title
            fig.update_layout(
                title_text=f"Topocentric frequency detections vs. Doppler predictions from {self.satellite} at {self.station}",
                font_family="lato",
                font_color="grey",
                title_font_size=18,
                title_font_color="blue",
                font_size=14,
                width=1200,
                height=600,
            )

            # Set y-axes titles
            fig.update_xaxes(title_text="<b>Time [s] </b>")
            fig.update_yaxes(title='<b>Frequency [MHz]</b>')
 
            # Save to disk the image
            fig.write_image(f'{self.session}_{self.station}_{self.satellite}_Predictions.pdf')           

# Do we need to update the TLE file?
current_time = time.time()
NEED_UPDATE: bool = False
tle_file: str = f'{PYSATTRACK}data/3le.tle'

if os.path.exists(tle_file):
    last_time = os.path.getmtime(tle_file)
    if(current_time - last_time > 3600 * 24):
        NEED_UPDATE = True
else:
    NEED_UPDATE = True

if NEED_UPDATE:
    print('TLE file is outdated - Downloading latest files from Space Track..')
    st = SpaceTrackClient(identity=siteCred['identity'], password=siteCred['password'])
    data = st.tle_latest(iter_lines=True, ordinal=1, epoch='>now-30', format='tle')
    #shutil.copy(tle_file, f'{tle_file}.{datetime.date.today().isoformat()}')
    with open(tle_file, 'w') as fp:
        for line in data:
            fp.write(line + '\n')
    print(f'Download complete - File save at {tle_file}')

sat = plotSpectra()
sat.data_process()

#if self.norad > 0:
#    sat.load_tle()

if output:
    sat.save_plots()
else:
    sat.create_plots()