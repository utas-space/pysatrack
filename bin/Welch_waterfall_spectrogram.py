#!/usr/bin/env python3
'''
welch waterfall spectrogram

Usage: welch_waterfall_spectrogram input_filename
'''
import argparse
from colorama import Fore, Style
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp

from scipy import signal
from scipy.fft import fftshift
from IPython.display import display
from pysctrack import core

parser = argparse.ArgumentParser()

parser.add_argument("filename", nargs="?",
                    help="input file", default="check_string_for_empty")
parser.add_argument("-dts", dest="int_time",
                    help="integration time", type=int, default = 10)
parser.add_argument("-nf", dest="nfft",
                    help="number of FFT points", type=int, default = 16384)
parser.add_argument("-bw", dest="bandwidth",
                    help="input bandwidth", type=float, default = 3150e3)
parser.add_argument("-cf", dest="centre_frequency",
                    help="sky centre frequency in Ettus", type=float, default = 7159.45e6)

args = parser.parse_args()

FNAME = args.filename

# segment_length_s is equivalent to integration time
segment_length_s = args.int_time   # in [s]

# Number of spectral points
welch_nperseg = args.nfft

# Read a memory-mapped files accessing only small segments on disk
# without reading the entire file into memory.
dfile2 = np.memmap(f"{FNAME}", dtype=np.csingle, mode='r')

input_bandwidth: float = args.bandwidth
central_frequency: float = args.centre_frequency

# Total number of samples
N = dfile2.shape[0]

# Sample Rate as the selected bandwidth because data are complex
sample_rate = input_bandwidth

# segment length in number of samples per integration time
segment_length = int(segment_length_s * sample_rate)

# Segment overlap in 0-1
segment_overlap_percent = 0

# Calgulate the offset in case we got overlap
segment_jump_by = int(segment_length * (1 - segment_overlap_percent))

# number of segments to split the data
segment_count = N // segment_jump_by

# Apodisation function
welch_window = 'hanning'

welch_overlap_percent = 0.50
welch_noverlap = int(welch_nperseg * welch_overlap_percent)

welch_specgram = np.zeros((segment_count, welch_nperseg))

# Print out the stats
print(f'{Fore.BLUE}')
print('# Initial setup')
print(f'integration time      : {segment_length_s} s')
print(f'frequency resolution  : {sample_rate / welch_nperseg} Hz')
print(f'number of spectra     : {segment_count}')
print(f'number spectral points: {welch_nperseg}')
print(f'{Style.RESET_ALL}')

xfc = np.zeros((segment_count, 3))
Smax = np.zeros(segment_count)
freq_detections = np.zeros(segment_count)

#from pysctrack import core
#print(f'segment_jump_by: {segment_jump_by}')
for n, i in enumerate(range(0, N - segment_length, segment_jump_by)):
    num_samples = segment_length if i + segment_length < N else N-i
    current_seg = dfile2[ i : i + num_samples]
    freq, welch_specgram_column = signal.welch(current_seg, fs=sample_rate,
                                               nperseg=welch_nperseg, noverlap=welch_noverlap, scaling='density', return_onesided=False)
    welch_specgram[n,:] = fftshift(welch_specgram_column)

    # calculate fdet
    xfc[n] = core.FindMax(welch_specgram_column, freq)
    freq_detections[n] = sample_rate / welch_nperseg * xfc[n, 1] + central_frequency

f,a = plt.subplots(figsize=(9, 8))

wspec_db = 10 * np.log10(welch_specgram)
x_freq = fftshift(freq) + central_frequency
y_time = np.arange(0, segment_count*segment_length_s, segment_length_s)
vmin, vmax = np.percentile(wspec_db, [10, 99])
vmin, vmax = None, None
x_extent = (x_freq[0], x_freq[-1])
y_extent = (y_time[0], y_time[-1])
dynspec = plt.imshow(wspec_db, cmap='magma', vmin=vmin, vmax=vmax,
                    extent=(*x_extent, *y_extent), origin='lower', interpolation='nearest')
plt.axis('auto')
plt.xlabel('Frequency / Hz')
plt.ylabel('Time / s')
#plt.xlim(7159.44e6, 7159.46e6)
cb = plt.colorbar(dynspec)
cb.set_label(r'Power / dB Hz$^{-1}$')
plt.show()

plt.plot(x_freq, wspec_db[0, :])
plt.title('Initial power spectrum')
plt.xlabel('Frequency [Hz]')
plt.ylabel('Power spectra [dB]')
plt.show()
#display(f)
#plt.close(f)

#f,a = plt.subplots(figsize=(9, 9))
#aveg_spec = welch_specgram.sum(axis=1)
#plt.plot(x_freq, aveg_spec)
#plt.axis('auto')
#plt.xlim(7159.449e6, 7159.451e6)
#plt.show()

#f,a = plt.subplots(figsize=(9, 8))
#plt.plot(Fdet, '.')
#plt.plot(x_freq, wspec_db[30])
#plt.xlim(7159.449e6, 7159.45e6)
#plt.show()

plt.plot(y_time, freq_detections, color='green', marker='.', linewidth=0)
plt.title('Topocentric frequency detections')
plt.xlabel('Time [s]')
plt.ylabel('Frequency [Hz]')
plt.show()
