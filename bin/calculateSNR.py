#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 10:49:40 2020

Calculate the first polynomial fit to the full bandwidth
of spectra.
v0.1 Copy calculateCpp to calculateSNR
@author: gofrito
"""

import argparse
import errno
import os
import re
import sys
import time

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from colorama import Back, Fore, Style
from mpl_toolkits.mplot3d import axes3d

from pysctrack import core, handler

mpl.rcParams['agg.path.chunksize'] = 10000

# take time
Tstart: float = time.process_time()

# find the directory where pysctrack and catalogues are installed
PYSCTRACK_DIR: str = os.path.dirname(os.path.realpath(__file__))[:-3]
PYSCTRACK_CAT: str = os.path.join(PYSCTRACK_DIR, 'cats')

class CalculateCpp:
    def __init__(self):
        # List of parsed parameters
        parser = argparse.ArgumentParser()
        parser.add_argument("-bw", dest="bandwidth", help="observing bandwidth", type=float, default=32)
        parser.add_argument("-ep", dest="epoch", help="specify your own epoch [yyyy.mm.dd]", type=str)
        parser.add_argument("-it", dest="integration_time", help="define integration when is a fraction of 1", type=int)
        parser.add_argument("-lf", dest="long_format", action="store_true", default=False)
        parser.add_argument("-db", dest="debug", action="store_true", default=False)
        parser.add_argument("-mk", "--mask", help="outline anomolous points on the data and do masking", action="store_true", default=False)
        parser.add_argument("-o", "--output", help="Store in a text file the averaged spectra", action="store_true", default=False)
        parser.add_argument("-p", "--plot", help="plot S/C signal with the aid of a menu", action="store_true", default=False)
        parser.add_argument("-p1", dest="polynomial", help="order of the first polynomial fit", type=int, default=6)
        parser.add_argument("-pc", "--power_centre", help="do NOT estimate the weighted peak position", action="store_true", default=False)
        parser.add_argument("-rf", dest="radio_frequency", help="initial sky freq of the channel", type=float, default=8400)
        parser.add_argument("-sc", dest="spacecraft", help="specify spacecraft ortherwise use filename", type=str)
        parser.add_argument("-sf", "--sky_frequency", help="change labels to show results in sky frequency", action="store_true", default=False)
        parser.add_argument("-t0", dest="ini_time", help="initial time within the scan", type=int, default=0)
        parser.add_argument("-t1", dest="end_time", help="end time within the scan", type=int, default=0)
        parser.add_argument("--version", action="version", version="%(prog)s 0.6")
        parser.add_argument("-vx", "--vexade", help="for fast S/C do not use SNR-based weighted functions", action="store_true", default=False)
        parser.add_argument("-zf", dest="zoom_frequency", help="search window boundaries in [MHz]", nargs="+", type=float, metavar="N", default=(4.7, 4.9))
        parser.add_argument("filename", nargs="?", help="spectra input file", default="check_string_for_empty")

        args = parser.parse_args()

        # Check the flags pre-selected in command line
        self.plot: bool = args.plot
        self.textout: bool = args.output
        self.vexade: bool = args.vexade
        self.mask: bool = args.mask
        self.debug: bool = args.debug
        self.skyFreq: bool = args.sky_frequency
        self.all_spectra: bool = args.long_format

        # By default we use the weighted power, set -pc to skip
        self.powcen: bool = True
        if args.power_centre:
            self.powcen = False

        # Set observation-dependant parameters
        self.t0: float = args.ini_time
        self.t1: float = args.end_time

        self.Npp: int = args.polynomial
        self.Npf: int = self.Npp - 1

        # Check if the file exists
        if os.path.exists(args.filename):
            path, self.filename = os.path.split(args.filename)
            self.path: str = f"{os.path.abspath(path)}/"
        else:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.filename)

        """
        Filename format : Session name + Station + Data Format + scan number + FFT points + integration time + channel + _swpec.bin
        Example: m200101_Mh_VDIF_No0001_3200000pt_5s_ch1_swspec.bin
        Split the file in 7 fields:
        """
        match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:N)?([oabcdefgh])?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_([^_]*)", self.filename, re.I)

        # Extract all observing params from the filename
        self.session: str = match.group(1)
        self.station: str = match.group(2)
        self.raw_format: str = match.group(3)
        self.ifband: str = match.group(4)
        self.scan_number: int = int(match.group(5))
        self.FFTpt: int = int(match.group(6))
        self.dts: int = int(match.group(7))
        self.channel: int = int(match.group(8))
        software: str = match.group(9)[0:6]

        # Read binary file to calculate rev 0 or 1
        if software == 'swspec':
            self.rev = 0
        elif software == 'scspec':
            self.rev = 1

        # If integration time was 0 - select it manually
        if args.integration_time:
            self.dts = args.integration_time

        # Find the spacecraft observed
        if args.spacecraft:
            self.spacecraft: str = args.spacecraft
        else:
            self.spacecraft: str = handler.assign_spacecraft(self.session[0])


        # Unless we force the settings manually
        if args.radio_frequency:
            self.Fstart: float = args.radio_frequency

        self.BW: float = args.bandwidth
        self.Fmin, self.Fmax = args.zoom_frequency

        # Convert to MHz
        self.BW *= 1e6
        self.Fmin *= 1e6
        self.Fmax *= 1e6

        # fill the epoch variable
        if args.epoch:
            self.epoch: str = args.epoch
        else:
            self.epoch: str = f"20{self.session[1:3]}.{self.session[3:5]}.{self.session[5:7]}"

        # if t1 = 0 use the entire file, otherwise use t1
        if self.t1:
            self.Nspec = int(self.t1 / self.dts)
        else:
            fsize: int = os.path.getsize(f"{self.path}{self.filename}")
            self.Nspec: int = int(np.floor(0.5 * fsize / (self.FFTpt + 1)))
            self.t1: float = self.Nspec * self.dts

    def data_process(self):
        SR: int = 2 * self.BW
        dtsam: float = 1 / SR
        self.df: float = SR / self.FFTpt
        Nfft: int = int(self.FFTpt / 2 + 1)
        jf: int = np.arange(Nfft)
        self.ff = self.df * jf

        # Select the initial and end spectra
        s0: int = round(self.t0 / self.dts)
        s1: int = self.Nspec
        tsp: float = self.dts * (0.5 + np.arange(s0, s1))
        self.Nspek: int = s1 - s0

        # If signal is too strong RMS is saturated.
        # Increase Hwin and Avoid x10 if the signal is too strong or wide.
        Hwin: int = int(1e4 / self.df)
        Avoid: int = int(1e3 / self.df)

        # Read file
        fd = open(f"{self.path}{self.filename}", "rb")

        # Set the search window
        self.bfs: int = int(self.Fmin / self.df)
        self.bfm: int = int(self.Fmax / self.df)

        # Initialize some vectors
        Sps: float = np.zeros((self.Nspek, self.bfm - self.bfs))
        self.Aspec: float = np.zeros(self.bfm - self.bfs)
        self.ffs: float = self.ff[self.bfs : self.bfm]
        self.fSpec: float = np.zeros((self.Nspek, Nfft))
        self.allSpec: float = np.zeros(Nfft)

        # Read the data and prepare the matrix
        for ip in range(self.Nspec):
            one_spec: float = np.fromfile(file=fd, dtype="float32", count=Nfft)
            # Only store values that are above t0 and until t1 (t1 == Nspec)
            if ip >= s0:
                #Do the Hanning in the full spectra or the chunk
                if True:
                    han_spec = one_spec * np.hanning(Nfft)
                    Sps[ip - s0] = han_spec[self.bfs : self.bfm]
                else:
                    Sps[ip - s0] = one_spec[self.bfs : self.bfm] * np.hanning(self.bfm - self.bfs)
                if self.all_spectra:
                    self.fSpec[ip-s0] += han_spec
                else:
                    self.allSpec += han_spec

        # close the opened file
        fd.close()

        '''
        Note: 02.03.2021
        For phase referencing we need to calculate the SNR from the section with S/C tone
        and do not use the section without signals. At the moment the SNR is calculated from 0 to t1,
        instead we need to do from t0-t1
        We use though Sps that laready contains s0> so Nspec is changed for Nspec
        '''
        self.Aspec = np.sum(Sps, axis=0) / self.Nspek
        mSp: float = Sps.max()
        self.Sps: float = Sps / mSp

        # Adding the full bw stack of spectrum
        if self.all_spectra:
            mfSpec: float = self.fSpec.mean()
            self.fSpec /= mfSpec
            print(f'\n{Fore.LIGHTRED_EX}DEGUG-Total size {int(sys.getsizeof(self.fSpec)/1024**2)} MB{Style.RESET_ALL}')
        else:
            # allSpec is the Sps version of the full averaged BW of data
            self.allSpec /= self.Nspek
            self.allSpec /= self.allSpec.mean()

        # Just copy the last one to lSpec for plotting purposes and normalise
        self.lSpec: float = one_spec
        if self.debug:
            print(f'\n{Fore.LIGHTRED_EX}DEGUG-noise floor: {self.lSpec.mean() / Nfft}{Style.RESET_ALL}')
        self.lSpec /= self.lSpec.mean()

        # If text output is enabled dump Sps to a text file
        # Make sure the window is small otherwise the file will be large
        if self.textout:
            epoch: str = f'20{self.session[1:3]}.{self.session[3:5]}.{self.session[5:7]}'
            output_fn: str = f'Spec.{self.spacecraft}.{epoch}.{self.station}.{self.scan_number:04d}.if{self.ifband}.txt'
            spec_header: str = f'Observation conducted on {epoch} at {self.station}\n\n\n'

            spectra = np.transpose([self.ffs, self.Aspec])
            np.savetxt(output_fn, spectra, newline="\n", header=spec_header)

        # Initialize vectors for determining the Doppler detections
        xfc: float = np.zeros((self.Nspek, 3))
        RMS: float = np.zeros((self.Nspek, 3))
        Smax: float = np.zeros(self.Nspek)
        self.SNR: float = np.zeros(self.Nspek)
        Fdet: float = np.zeros(self.Nspek)
        dxc: float = np.zeros(self.Nspek)
        MJD: int = np.zeros(self.Nspek)

        zbins: int = self.bfm - self.bfs
        avbin: float = 0

        # Seeking for the Max and estimating the RMS
        for ip in range(self.Nspek):
            xfc[ip] = core.FindMax(self.Sps[ip], self.ffs)
            Smax[ip] = xfc[ip, 2]
            Fdet[ip] = self.df * xfc[ip, 1] + self.ffs[0]
            RMS[ip] = core.GetRMS(self.Sps[ip], xfc[ip, 1], Hwin, Avoid)
            self.SNR[ip] = (xfc[ip, 2] - RMS[ip, 0]) / RMS[ip, 1]

            # Calculate the centre power of the signal with 1, 3, 5 or 7 bins
            if self.powcen:
                if (xfc[ip, 0]) == 1 or (xfc[ip, 0] == zbins - 1):
                    dxc[ip] = core.PowCenter(self.Sps[ip], xfc[ip, 1], 1)
                elif (xfc[ip, 0]) == 2 or (xfc[ip, 0] == zbins - 2):
                    dxc[ip] = core.PowCenter(self.Sps[ip], xfc[ip, 1], 2)
                else:
                    dxc[ip] = core.PowCenter(self.Sps[ip], xfc[ip, 1], 3)

        if self.debug:
            print(f'{Fore.LIGHTRED_EX}DEGUG-noise floor RMS: {RMS.mean()}{Style.RESET_ALL}')


        # Storing the initial bin for latter
        ibin: int = xfc[0, 1]

        # Adding the correction to our Frequency detections
        mSNR = self.SNR.mean()
        dxc *= self.df

        # if power centre True add PowCenter correction
        if self.powcen:
            FdetC = Fdet + dxc
        else:
            FdetC = Fdet

        # If -vx selected use Weight of 1s
        # By default use Weight-mask based on the SNR
        Weight: float = np.power(self.SNR, 2) / mSNR

        self.Fdet: float = FdetC

        # Initialize the Polynomial coefficients
        '''
        Cf:  Frequency polynomials
        Cfs: Frequency polynomials per second
        Cps: Phase polynomials per second
        Cpp: Phase polynomials per radian
        '''
        Cfs: float = np.zeros(self.Npp)
        Cps: float = np.zeros(self.Npp + 1)
        Cpp: float = np.zeros(self.Npp + 1)

        # Take into account if ini time different than 0
        tsp -= self.t0
        Tspan: float = tsp.max()

        # Calculate polys and fit
        Cf: float = core.PolyfitW1C(tsp, FdetC, Weight, self.Npf)
        Ffit: float = core.PolyfitW1(tsp, FdetC, Weight, self.Npf)

        # Estimate the residual frequency
        self.rFit = FdetC - Ffit
        self.Ffit = Ffit

        # Calculate the phase polynomials
        for jpf in range(self.Npp):
            Cfs[jpf] = Cf[jpf] * np.power(Tspan, -jpf)

        # Calculate all polynomials set
        for jpf in range(1, self.Npp + 1):
            Cps[jpf] = 2 * np.pi * Cfs[jpf - 1] / jpf
            Cpp[jpf] = Cps[jpf] * np.power(dtsam, jpf)

        # Save the polynomial coefficients into a file
        basefilen: str = np.size(self.filename) - 12  # 12 char: _swspec.bin
        cppname: str = f"{self.path}{self.filename[0:basefilen]}.poly{self.Npp}.txt"
        cfsname: str = f"{self.path}{self.filename[0:basefilen]}.X{self.Npf}cfs.txt"

        # Save the polynomials to files
        np.savetxt(cppname, Cpp)
        np.savetxt(cfsname, Cfs)

        # Reading starting time of the scan
        timebin: str = f"{self.path}{self.filename[0:basefilen]}_starttiming.txt"
        Start: float = 0
        if os.path.exists(timebin):
            Tsinfo = np.loadtxt(timebin, skiprows=1)
            MJD[:] = Tsinfo[0]
            Start = Tsinfo[1]

        # Save the frequency detections revision 0 into a file
        ttsp: float = tsp + Start + self.t0
        self.tsp: float = tsp + self.t0
        self.fdets = np.array([MJD, ttsp, self.SNR, Smax, FdetC, self.rFit])

        handler.write_fdets(self, self.rev)

        skyFreq: float = self.Fstart + FdetC[0] * 1e-6

        # Print out the stats
        print(f'{Fore.BLUE}')
        print(f"- S/C sky frequency   : {skyFreq:.2f} MHz ({skyFreq * 1e6:.3f}) Hz")
        print(f"- S/C band frequency  : {FdetC[0] / 1e6:.3f} MHz ({FdetC[0]:.3f}) Hz")
        print(f"- S/C initial bin     : {ibin:.2f} ")
        print(f"- Total nr of spectra : {self.Nspec} ")
        print(f"- Nr of spectra used  : {self.Nspek} ")
        print(f"- Average SNR         : {mSNR:.2f} ")
        print(f"- Doppler noise       : {self.rFit.std():.3f} Hz")
        print(f"- Doppler shift       : {np.abs(FdetC[0] - FdetC[-1]):.2f} Hz")
        print(f'{Style.RESET_ALL}')

        # Save in a file the stats
        # handler.write_stats(self, self.rev)

    def create_plots(self):
        if self.plot:

            # Select sky fequency or baseband -
            if self.skyFreq:
                self.ff += self.Fstart * 1e6
                self.Fdet += self.Fstart * 1e6

            self.ff /= 1e6
            self.Fdet /= 1e6
            self.Ffit /= 1e6

            print(f"Checking the output spectra from swspectrometer")
            print(f"Select the figure that you would like to display")
            print(f"1- Last spectrum from the scan - full bandwdith")
            print(f"2- Averaged time-integrated spectra - zoom window")
            print(f"3- Frequency detections of the S/C signal")
            print(f"4- Summary of the spectra and signal detected")
            print(f"5- Dynamic spectra")
            # print(f"6- 3D dynamic spectra")
            graph = input("Make a choice: 1-2-3-4-5-6\n")

            plt.figure(graph)

            if graph == "1":
                plt.semilogy(self.ff, self.lSpec, color="deepskyblue", label="spectral power")
                plt.ylabel("Spectral power")
                plt.xlabel("Frequency [MHz]")
                plt.grid('on')
                plt.title(f"Last spectrum from the session {self.session} at {self.station}")
                plt.show()

            elif graph == "2":
                plt.semilogy(self.ff[self.bfs : self.bfm], self.Aspec, color="deepskyblue", label="spectral power")
                plt.ylabel("Spectral power")
                plt.xlabel("Frequency [MHz]")
                plt.title(f"Averaged time-integrated spectra {self.session} at {self.station}")
                plt.show()

            elif graph == "3":
                ax1 = plt.subplot2grid((3, 1), (0, 0))
                plt.title(f"Summary I of the Frequency detections {self.session} at {self.station}")
                ax1.plot(self.tsp, self.SNR, color="indigo", marker="x", linestyle="none")
                ax1.set_ylabel("SNR")
                ax2 = plt.subplot2grid((3, 1), (1, 0))
                ax2.plot(self.tsp, self.Fdet, color="mediumseagreen", marker="o", linestyle="none")
                ax2.plot(self.tsp, self.Ffit, color='red', marker='.', linestyle='none')
                ax2.set_ylabel("Fdets [MHz]")
                ax3 = plt.subplot2grid((3, 1), (2, 0))
                ax3.plot(self.tsp, self.rFit, color="orangered", marker="x", linestyle="none")
                ax3.set_ylabel("Dnoise [Hz]")
                ax3.set_xlabel("Time [s]")
                plt.tight_layout()
                plt.show()

            elif graph == "4":
                ax1 = plt.subplot2grid((2, 2), (0, 0), colspan=2)
                plt.title(f"Summary II of the Frequency detections {self.session} at {self.station}")
                ax1.semilogy(self.ff, self.lSpec, color="blue", label="spectral power")
                ax1.set_xlabel("Frequency [MHz]")
                ax1.set_ylabel("Spectral power")
                ax2 = plt.subplot2grid((2, 2), (1, 0))
                ax2.semilogy(self.ff[self.bfs : self.bfm], self.Aspec, color="blue")
                ax2.set_ylabel("Spectra power")
                ax2.set_xlabel("Frequency [MHz]")
                ax3 = plt.subplot2grid((2, 2), (1, 1))
                ax3.plot(self.tsp, self.Fdet, color="mediumseagreen", marker="x", linestyle="none")
                ax3.set_ylabel("Fdets [MHz]")
                ax3.set_xlabel("Time [s]")
                plt.tight_layout()
                plt.show()

            elif graph == "5":
                dynspec = plt.pcolormesh(self.tsp, self.ffs, np.transpose(10 * np.log10(self.Sps)), cmap='seismic', shading='auto')
                plt.colorbar(dynspec)
                plt.ylabel(f'Frequency [MHz]')
                plt.xlabel(f'Time [s]')
                plt.title(f'Dynamic spectrum {self.session} at {self.station}')
                plt.show()

            elif graph == "6":
                ax = plt.axes(projection="3d")
                #x = np.zeros((self.Nspek, self.bfm - self.bfs))
                #y = np.zeros((self.Nspek, self.bfm - self.bfs))
                #z = np.zeros((self.Nspek, len(x)))
                #for ip in range(self.Nspek):
                #    x[ip] = self.ffs
                #    y[ip] = self.Sps[ip,:]
                #    z[ip] = np.ones(len(x)) * ip
                #ax.plot3D(x, y, np.log10(z), cmap='hsv')          
                dynspec = ax.pcolormesh(self.tsp, self.ffs, np.transpose(10 * np.log10(self.Sps)), cmap='seismic', shading='auto')
                #plt.plot(self.ffs, self.tsp, np.log10(self.Sps), cmap='hsv')
                plt.colorbar(dynspec)
                ax.set_xlabel('Frequency [MHz]')
                ax.set_ylabel('Time [s]')
                plt.title('3D dynaminc spectra')
                #ax.set_xlim(self.bfs, self.bfm)
                plt.show()
            return graph


first_polynomials = CalculateCpp()
first_polynomials.data_process()
print(f'{Fore.GREEN}* DEBUG-Total time    : {time.process_time() - Tstart:.2} s{Style.RESET_ALL}\n')
first_polynomials.create_plots()
