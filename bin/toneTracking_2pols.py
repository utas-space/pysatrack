#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 20:41:24 2022

@author: gofrito
"""

import argparse
import errno
import os
import re
import time

import numpy as np
import pandas as pd
import scipy.signal as sps
import scipy.stats as stats
import plotly.graph_objects as go

from colorama import Fore
from astropy.time import Time, TimeDelta
from matplotlib import pyplot as plt
from pysctrack import core, handler

def MakeSpec(filename, Nspec, Nfft, Nav, Ovlp, Win, Padd, dpadd):
    """
    Calculate the spectra from the given file with the temporal evolution of the carrier tone

    Parameters
    ----------
    filename : string
        The spectra recorded in binary format from the tone file
    Nspec : int
        Number of averaged spectrum to perform
    Nfft : int
        The number of spectral points to use for the transformation
    Nav : int
        Number of averaged spectra
    Ovlp : int
        overlap factor.
    Win : array_like
        Spectral window to apply in the FFT.
    Padd : int
        Padding factor on the FFT.
    dpadd : array_like
        padding array.

    Returns
    -------
    spa : array_like
        returns the array of averaged windowed-overlapped spectra.

    """
    Nfp: int = int(Nfft * Padd / 2 + 1)
    spa: float = np.zeros((Nspec, Nfp))
    Nspav: int = int(Nav * Ovlp - (Ovlp - 1))
    Bav: int = Nav * Nfft

    fd = open(filename, "rb")
    dinC = np.fromfile(file=fd, dtype="float32", count=int(2 * (Bav * (Nspec + 1) + 1)))
    tp = dinC[::2] + 1j * dinC[1::2]

    for ip in np.arange(Nspec):
        for jp in np.arange(Nspav):
            abin = int(Bav * ip + jp * Nfft / Ovlp)
            bbin = int(abin + Nfft)
            din = tp[abin:bbin]
            din = np.multiply(din, Win)
            if Padd > 1:
                dinp = np.append(din, dpadd)
            else:
                dinp = din
            sp = np.fft.fft(dinp)

            spa[ip, :] = spa[ip, :] + np.power(np.real(sp[0 : Nfp]), 2) + np.power(np.imag(sp[0 : Nfp]), 2)

    fd.close()
    return spa

class tone_tracking:
    def __init__(self):
        # List of parsed parameters
        parser = argparse.ArgumentParser()
        parser.add_argument("filename", nargs="?", help="Input spectra file")
        parser.add_argument("-bd", dest="frequencyband", help="Frequncy band being observed", type=str, default='X-band')
        parser.add_argument("-bw", dest="bandwidth", help="Bandwidth", type=float, default=2000)
        parser.add_argument("-bw2", dest="bandwidthStep2", help="Bandwidth used with 2nd fit", type=float, default=50)
        parser.add_argument("-ep", dest="epoch", help="specify your own epoch - format ex. 2020.02.02", type=str)
        parser.add_argument("-nav", dest="nav", help="Number of averaged FFT", type=float, default=2)
        parser.add_argument("-nf", dest="nfft", help="Number of FFT points", type=int, default=40000)
        parser.add_argument("-ol", dest="overlap", help="FFT overlapping factor", type=float, default=2)
        parser.add_argument("-p", "--plot", help="Plot a summary of the results on screen", action="count", default=False)
        parser.add_argument("-pa", dest="padding", help="Padding factor", type=float, default=2)
        parser.add_argument("-pc", "--powercentre", help="Do not use an neighbour average to estimate Frequency Max", action="store_true", default=True)
        parser.add_argument("-s", dest="saveplot", help="Store the plot in a file", type=int, default=0)
        parser.add_argument('-sc', dest='target', help='Target of the observation', type=str)
        parser.add_argument("-rf", dest="radiofrequency", help="Starting Radio Frequency", type=float, default=0)
        parser.add_argument("-ta", dest="addtime", help="Time added in SCtracker", type=float, default=0)
        parser.add_argument("-t0", dest="starttime", help="Seconds to skip in dPLL", type=float, default=0)
        parser.add_argument("-t1", dest="endtime", help="Total number of seconds in dPLL", type=float, default=0)
        parser.add_argument("-mask", dest="mask_file", help="Input the ON/OFF mask file", type=str)

        args = parser.parse_args()

        self.powcen: bool = args.powercentre
        self.Tadd: int = args.addtime
        self.Tskip: int = args.starttime
        self.Nfft: int = args.nfft

        self.target = args.target
        self.band: str = args.frequencyband

        # Settings related to PLL
        self.BW: int = args.bandwidth
        self.BW2: int = args.bandwidthStep2
        self.Ovlp: int = args.overlap
        self.Nav: int = args.nav
        self.Padd: int = args.padding
        self.Fstart: float = args.radiofrequency
        self.mask_file: str = args.mask_file

        # Define ooutput level
        self.verbose: bool = False
        self.fverbose: bool = False
        self.file_plot: int = args.saveplot

        if args.plot == 1:
            self.verbose: bool = True
        elif args.plot > 1:
            self.fverbose: bool = True

        # Check if the file exists
        if os.path.exists(args.filename):
            path, self.filename = os.path.split(args.filename)
            self.path: str = f"{os.path.abspath(path)}/"
            self.fil_lng: int = len(self.filename) - 10
        else:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.filename)

        if args.mask_file == None:
            self.apply_mask: bool = False
        else:
            self.apply_mask: bool = True
            if os.path.exists(args.mask_file):
                self.mask_file = args.mask_file
            else:
                raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.filename)

        # Read settings used in sctracker
        match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*))_([^_]*)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_tone", self.filename, re.I)

        # Assuming std nomenclature assign settings
        self.session: str = match.group(1)
        self.station: str = match.group(2)
        self.raw_format: str = match.group(3)
        self.scan_number: int = int(match.group(4)[2:6])
        self.scanMode: str = match.group(4)[0:2]
        self.FFT: int = match.group(5)
        self.ints: int = match.group(6)
        self.channel: int = match.group(7)

        # Estimate the number of samples in the file
        fsize: float = os.path.getsize(self.filename)

        # Use t1 or total number of samples
        if args.endtime:
            self.Tspan: float = args.endtime
            Nt: int = self.Tspan * 2 * 8 * self.BW
        else:
            self.Tspan: float = fsize / (2 * 8 * self.BW)
            Nt: int = int(fsize / 8)

        # Debug info on screen
        print(f'Settings')
        print(f"Length of the file   : {self.Tspan} sec")
        print(f"Size of the file     : {Nt} samples")

        # Make sure that is a finite number of samples
        dts: float = self.Nfft / self.BW
        self.Tspan = np.floor(self.Tspan / dts) * dts

        # Set the epoch manually
        if args.epoch:
            self.epoch: str = args.epoch
        else:
            self.epoch: str = f"20{self.session[1:3]}.{self.session[3:5]}.{self.session[5:7]}"

    def data_processing(self):
        # inialise variables
        SR: int = 2 * self.BW           # sampling rate
        Nt: int = int(self.Tspan * SR)  # total number of samples

        dt: float = 1 / SR              # sampling interval
        df: float = SR / self.Nfft      # frequency resolution of the FFT
        tw: float = dt * self.Nfft      # filter set up time

        jt: float = np.arange(Nt)
        tt: float = jt * dt             # input time grid

        # save variables for plotting and storing the output files
        self.dts: float = dt
        self.df: float = df

        # Initialise more variables
        Nspec: int = int(Nt / (self.Nfft * self.Nav))
        sk: int = int(self.Tskip * df / 2)
        Nspek: int = Nspec - sk
        jspek: int = np.linspace(sk, Nspec, Nspek)
        Bav: int = self.Nfft * self.Nav
        tspek: float = (jspek + 0.5) * Bav * dt
        Npadd: int = self.Nfft * (self.Padd - 1)
        dpadd: int = np.zeros(Npadd)

        #tps: int = np.arange(0, self.Tspan, int(self.Tspan / Nspec))

        #print(f'Skip # of spectra : {sk}')
        print(f'Total # of spectra   : {Nspec}')
        print(f'Frequency resolution : {self.df} Hz')
        print(f'Sampling interval    : {1e3 * self.dts} ms')
        print(f'Integration time     : {int(self.Tspan/Nspec)} s')

        print(f'\n# Measurements')
        tone_x: str = f'{self.path}{self.filename}'

        # Create full timestamp
        time_file: str = tone_x.replace('tone0.bin', 'starttiming.txt')
        initial_ts = np.loadtxt(time_file, skiprows=1)
        Tstart = Time(initial_ts[0], format='mjd') + TimeDelta(initial_ts[1], format='sec')
        print(f'Scan start time      : {Tstart.isot}')
        
        # Tscan is a copy of Tstart to search for transmitting times
        Tscan = Tstart
        dt = TimeDelta(int(self.Tspan/Nspec), format='sec')

        # Create an apodisation function for the FFT
        jps: int = np.arange(self.Nfft)
        Win: float = np.power(np.cos(np.pi / self.Nfft * (jps - 0.5 * self.Nfft + 0.5)), 2)

        Nfp: float = self.Nfft * self.Padd / 2 + 1  #
        jfs: float = np.arange(Nfp)
        dfs: float = 1 / (tw * self.Padd)
        ffs: float = jfs * dfs

        if self.scanMode == 'Nc':
            tone_y = tone_x.replace('Nc', 'Nd')
        elif self.scanMode == 'Nd':
            tone_y = tone_x.replace('Nd', 'Nc')
        elif self.scanMode == 'Ne':
            tone_y = tone_x.replace('Ne', 'Nf')
        elif self.scanMode == 'Nf':
            tone_y = tone_x.replace('Nf', 'Ne')
        else:
            print('Error in the input format - at the moment only support IF C to F')

        Sp_x: float = MakeSpec(tone_x, Nspec, self.Nfft, self.Nav, self.Ovlp, Win, self.Padd, dpadd)
        Sp_y: float = MakeSpec(tone_y, Nspec, self.Nfft, self.Nav, self.Ovlp, Win, self.Padd, dpadd)
        
        # If apply mask is True
        if self.apply_mask:
            mask = pd.read_csv(self.mask_file, names=['status', 'time'], delim_whitespace=True)
            Sp_polh = []
            Sp_polv = []
            vjp = 0
            mjp = 0

            for ip in range(len(mask)-1):
                if mask.status[ip] == 'TXRON':
                    store_value: bool = True
                    stop_time = Time(mask['time'][ip+1])
                elif mask.status[ip] == 'TXROFF':
                    store_value: bool = False
                    stop_time = Time(mask['time'][ip+1])
                while Tscan < stop_time and mjp < np.shape(Sp_x)[0]:
                    if store_value:
                        'life does not continue'
                        Sp_polh.append(Sp_x[mjp, :])
                        Sp_polv.append(Sp_y[mjp, :])
                        vjp += 1
                    else:
                        'life continues'
                    Tscan += dt
                    mjp += 1
            
            print(f'Total number of valids scans {vjp} of total {mjp}..')
        else:
            Sp_polh = Sp_x
            Sp_polv = Sp_y
            vjp = Nfp

        jspek: float = np.arange(0, vjp)
        #tspek: float = (jspek + 0.5) * int(self.Tspan/Nspec)

        # Add both polarisation matrices to Sp (masked) and Sp_full (all)
        Sp = np.add(np.array(Sp_polh), np.array(Sp_polv))
        Sp_full = np.add(np.array(Sp_x), np.array(Sp_y))

        # Calculate the mean value
        xSp: float = Sp.mean()

        # Normalise everything
        Sp = np.divide(Sp, xSp)

        # Calculate the averaged with all the spectra
        Spa: float = np.divide(Sp.sum(axis=0), Nspec)

        bmin: int = int(0.1 * self.Nfft)
        bmax: int = int(0.4 * self.Nfft)

        noise_floor = Spa[bmin : bmax].mean()

        # Calculate the total power for sigma > 3
        low_lim = 0
        up_lim = self.Nfft
        found = 0
        finish = 0

        for ip in range(self.Nfft):
            if finish == 0:
                if (Spa[ip] > (noise_floor * 5)) and (found == 0):
                    low_lim = ip
                    found = 1
                if found == 1:
                    if (Spa[ip] < (noise_floor * 5)):
                        up_lim = ip - 1
                        found = 0
                        finish = 1

        SNR = Spa[low_lim : up_lim].sum() / noise_floor / Nspec
        SNR2 = (Spa[low_lim : up_lim].sum() - noise_floor) / Spa[bmin : bmax].std()

        print(f'SNR method 1: {SNR:.2f} - {10 * np.log10(SNR):.1f} dB')
        print(f'SNR method 2: {SNR2:.2f} - {10 * np.log10(SNR2):.1f} dB')

        b0 = int((self.BW - self.BW2 )/ self.df )
        b1 = int((self.BW + self.BW2 )/ self.df )

        if self.verbose:
            avg_sp = Spa[bmin : bmax].mean()
            min_sp = Spa[bmin : bmax].min()
            max_sp = Spa[bmin : bmax].max()

            plt.figure(num = 0, figsize=[6.4, 4.8])
            plt.semilogy(ffs, Spa, color='blue')
            plt.xlim([0, self.BW])
            plt.axhline(y=Spa[bmin : bmax].mean(), color="red", linestyle="--")
            plt.axvline(x=ffs[low_lim], color='red', linestyle='-')
            plt.axvline(x=ffs[up_lim], color='red', linestyle='-')
            plt.ylim([0.995 * min_sp , 1.2 * max_sp])
            plt.ylabel("Power spectrum")
            plt.xlabel("Frequency [Hz]")
            plt.title(f"Step 1 - Full stacked spectra")
            plt.show()

            plt.figure(num = 1)
            plt.title(f"Step 2 - Zoom to the full stacked spectra")
            plt.semilogy(ffs[b0 : b1], Spa[b0 : b1], marker=".", linestyle=None)
            plt.semilogy(ffs[b0 : b1], sps.savgol_filter(Spa[b0 : b1], 31, 3), linestyle='solid')
            plt.ylabel("Power spectrum")
            plt.xlabel("Frequency [Hz]")
            plt.axhline(y=Spa[bmin : bmax].mean(), color="red", linestyle="--")
            plt.axvline(x=ffs[low_lim - b0], color='red', linestyle='-')
            plt.axvline(x=ffs[up_lim - b0], color='red', linestyle='-')
            plt.ylim([0.995 * min_sp , 1.2 * max_sp])
            plt.show()

            plt.figure(num = 2)
            #Note -> it does only work with the full matrix
            dynspec = plt.pcolormesh(tspek, ffs[b0 : b1], np.transpose(10 * np.log10(Sp_full[:, b0 : b1])), cmap='magma', shading='auto')
            cb = plt.colorbar(dynspec)
            cb.set_label(r'Power / dB Hz$^{-1}$')
            plt.ylabel(f'Frequency [MHz]')
            plt.xlabel(f'Time [s]')
            plt.title(f'Step 3 - Dynamic spectrum')
            plt.show()

        if self.file_plot == 1:
            fig = go.Figure()
            fig.add_traces(go.Scatter(mode='lines', x=ffs, y=10 * np.log10(Spa)))

            # Add figure title
            fig.update_layout(
                title_text=f"Normalised power spectra from {self.target} on {self.epoch} at {self.station}<br><sup>The target was observed at {self.Fstart}</sup>",
                font_family="lato",
                font_color="grey",
                title_font_size=18,
                title_font_color="blue",
                font_size=15,
                width=1200,
                height=600,
            )
            fig.add_hline(y=10 * np.log10(Spa[bmin : bmax].mean()), line_color="red", line_width=3, line={'dash': 'dash'})
            fig.add_vline(x=ffs[low_lim], line_color='red', line_width=2, line={'dash': 'solid'})
            fig.add_vline(x=ffs[up_lim], line_color='red', line_width=2, line={'dash': 'solid'})

            # Set axes titles
            fig.update_xaxes(title_text="<b>Narrow band [Hz] </b>", range=[0, self.BW])
            fig.update_yaxes(title='<b>Spectral power [dB]</b>')

            # Save to disk the image
            fig.write_image(f'{self.path}{self.session}_{self.station}_{self.target}_Spectra.pdf')

        elif self.file_plot == 2:
            fig = go.Figure()
            fig.add_traces(go.Scatter(mode='markers', x=ffs, y=10 * np.log10(Spa)))
            fig.add_traces(go.Scatter(mode='lines', x=ffs, y=10 * np.log10(sps.savgol_filter(Spa, 31, 3))))

            # Add figure title
            fig.update_layout(
                title_text=f"Normalised power spectra from {self.target} on {self.epoch} at {self.station}<br><sup>The target was observed at {self.Fstart}</sup>",
                font_family="lato",
                font_color="grey",
                title_font_size=18,
                title_font_color="blue",
                font_size=15,
                width=1200,
                height=600,
            )

            fig.add_hline(y=10 * np.log10(Spa[bmin : bmax].mean()), line_color="red", line={'dash':'dash'})
            fig.add_vline(x=ffs[low_lim], line_color='red', line={'dash':'solid'})
            fig.add_vline(x=ffs[up_lim], line_color='red', line={'dash':'solid'})

            # Set axes titles
            fig.update_xaxes(title_text="<b>Zoomed frequency band [Hz]</b>", range=[b0, b1])
            fig.update_yaxes(title='<b>Spectral power [dB]</b>')

            # Save to disk the image
            fig.write_image(f'{self.path}{self.session}_{self.station}_{self.target}_zoomSpectra.pdf')

        elif self.file_plot == 3:
            fig = go.Figure()

            fig.add_traces(go.Heatmap(x = ffs[b0 : b1], y = tspek, z = 10 * np.log10(Sp[:, b0 : b1]), colorscale = 'Electric'))

            # Add figure title
            fig.update_layout(
                title_text=f"Spectral waterfall of the carrier signal from {self.target} on {self.epoch} at {self.station}",
                font_family="lato",
                font_color="grey",
                title_font_size=18,
                title_font_color="blue",
                font_size=14,
                width=1200,
                height=600,
            )

            # Set y-axes titles
            fig.update_xaxes(title_text="<b>Zoomed frequency band [Hz] </b>")
            fig.update_yaxes(title='<b>Time [s]</b>')

            # Save to disk the image
            fig.write_image(f'{self.path}{self.session}_{self.station}_{self.target}_Waterfall.pdf')

start: float = time.process_time()

print('\n1.- initialising user parameters..')
echo = tone_tracking()

print('\n2.- Calculating the spectral power from the tone file..')
echo.data_processing()

print(f'3.- Total data processing time {time.process_time() - start:.2f} s')
