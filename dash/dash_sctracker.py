# -*- coding: utf-8 -*-
"""
Created on Wed Feb  1 12:12:12 2023

@author: matil
"""

import pandas as pd
from skyfield.api import load
import numpy as np
import plotly.graph_objects as go
from dash import Dash, html, dcc, Input, Output, callback
import plotly.express as px
from astroquery.jplhorizons import Horizons
from astropy import time
from datetime import timedelta

app = Dash(__name__, routes_pathname_prefix='/dash/')

craft = 'Not Selected'
#Plotting Functions
#Orbits
def plot_orbit(radius, colour, fig):
    fig.add_shape(type='circle',
             xref='x', yref='y',
             x0=-radius, y0=-radius, x1=radius, y1=radius,
             line=dict(
                width=2, color=colour),
             layer = 'below')

#Planets
def plot_planet(x, y, size, colour, name, fig):
    fig.add_trace(go.Scatter(x=np.array(x), y=np.array(y),
                        mode='markers',
                        marker_size=size,
                        marker_color=colour,
                        name ='',
                        hovertemplate =
                        '<b>'+name+'</b>'+
                        '<br>(%{x}, %{y}) au'))

#Space Craft
def plot_spacecraft(x, y, name, fig):
    fig.add_trace(go.Scatter(x=np.array(x), y=np.array(y),
                        mode='markers',
                        marker_size=8, marker_symbol='star-diamond',
                        marker_color='#BBC7CF', 
                        name='',
                        hovertemplate =
                        '<b>'+name+'</b>'+
                        '<br>(%{x}, %{y}) au'))
    
#Annontations
def annotate(x, y, name, fig):
    fig.add_annotation(x=np.array(x), y=np.array(y),
                  text=name, showarrow=False,
                  font=dict(
                      family = 'calibri',
                      size=14,
                      color='#BBC7CF'))


def solar_system_map():
    #Planet Data
    planets = load('de421.bsp')

    #Timescale and current time
    ts = load.timescale()
    t = ts.now() + timedelta(days=1)
    tdb_julian = t.tdb
    
    #Load the planet data
    sun, earth, mars, venus, mercury, jupiter = planets['sun'], planets['earth'], planets['mars'], planets['venus'], planets['mercury'], planets['jupiter barycenter']
    
    #Barycentric positions
    b_sun = sun.at(t)
    b_earth = earth.at(t)
    b_mars = mars.at(t)
    b_venus = venus.at(t)
    b_mercury = mercury.at(t)
    b_jupiter = jupiter.at(t)
    
    #Planet positions
    sun_x, sun_y = b_sun.position.au[0], b_sun.position.au[1]
    earth_x, earth_y = b_earth.position.au[0], b_earth.position.au[1]
    mars_x, mars_y = b_mars.position.au[0], b_mars.position.au[1]
    venus_x, venus_y = b_venus.position.au[0], b_venus.position.au[1]
    mercury_x, mercury_y = b_mercury.position.au[0], b_mercury.position.au[1]
    jupiter_x, jupiter_y = b_jupiter.position.au[0], b_jupiter.position.au[1]
    
    #x values
    x_s = sun_x
    x_e = earth_x
    x_m = mars_x
    x_v = venus_x
    x_me = mercury_x
    x_j = jupiter_x

    #y values
    y_s = sun_y
    y_e = earth_y
    y_m = mars_y
    y_v = venus_y
    y_me = mercury_y
    y_j = jupiter_y
    
    #Total x-y distance from the barycenter
    xy_sun = np.sqrt((np.power(b_sun.position.au[0],2)) + (np.power(b_sun.position.au[1],2)))
    xy_earth = np.sqrt((np.power(b_earth.position.au[0],2)) + (np.power(b_earth.position.au[1],2)))
    xy_mars = np.sqrt((np.power(b_mars.position.au[0],2)) + (np.power(b_mars.position.au[1],2)))
    xy_venus = np.sqrt((np.power(b_venus.position.au[0],2)) + (np.power(b_venus.position.au[1],2)))
    xy_mercury = np.sqrt((np.power(b_mercury.position.au[0],2)) + (np.power(b_mercury.position.au[1],2)))
    xy_jupiter = np.sqrt((np.power(b_jupiter.position.au[0],2)) + (np.power(b_jupiter.position.au[1],2)))
    jupiter_half = xy_jupiter/2
    
    #General Information
    loc = '500@0'
    t_inst = tdb_julian

    #Space craft ID
    bep = '-121'
    osi = '-64'
    mex = '-41'
    psp = '-96'
    sol = '-144'
    jaw = '-170'
    jun = '-61'
    haya = '-37'
    aka = '-5'
    jui = '-28'
    
    #Positions
    #BepiColombo
    bep_position = Horizons(id=bep, location=loc, epochs=t_inst)
    bep_vec = bep_position.vectors()
    
    #OsirisRex
    osi_position = Horizons(id=osi, location=loc, epochs=t_inst)
    osi_vec = osi_position.vectors()
    
    #Mars Express
    mex_position = Horizons(id=mex, location=loc, epochs=t_inst)
    mex_vec = mex_position.vectors()
    
    #Parker Solar Probe
    psp_position = Horizons(id=psp, location=loc, epochs=t_inst)
    psp_vec = psp_position.vectors()
    
    #Solar Orbiter
    sol_position = Horizons(id=sol, location=loc, epochs=t_inst)
    sol_vec = sol_position.vectors()
    
    #James Webb Space Telescope
    jaw_position = Horizons(id=jaw, location=loc, epochs=t_inst)
    jaw_vec = jaw_position.vectors()
    
    #Juno
    jun_position = Horizons(id=jun, location=loc, epochs=t_inst)
    jun_vec = jun_position.vectors()

    #Juice
    jui_position = Horizons(id=jui, location=loc, epochs=t_inst)
    jui_vec = jui_position.vectors()

    #Hayabusa2
    haya_position = Horizons(id=haya, location=loc, epochs=t_inst)
    haya_vec = haya_position.vectors()
    
    #Akatsuki
    aka_position = Horizons(id=aka, location=loc, epochs=t_inst)
    aka_vec = aka_position.vectors()
    
    #x and y components
    x_bep, y_bep = bep_vec['x'][0], bep_vec['y'][0]
    x_osi, y_osi = osi_vec['x'][0], osi_vec['y'][0]
    x_mex, y_mex = mex_vec['x'][0], mex_vec['y'][0]
    x_psp, y_psp = psp_vec['x'][0], psp_vec['y'][0]
    x_sol, y_sol = sol_vec['x'][0], sol_vec['y'][0]
    x_jaw, y_jaw = jaw_vec['x'][0], jaw_vec['y'][0]
    x_jun, y_jun = jun_vec['x'][0], jun_vec['y'][0]
    x_jui, y_jui = jui_vec['x'][0], jui_vec['y'][0]
    x_haya, y_haya = haya_vec['x'][0], haya_vec['y'][0]
    x_aka, y_aka = aka_vec['x'][0], aka_vec['y'][0]
        
    #Plot information
    fig = go.Figure()
    fig.update_xaxes(showgrid=False, zeroline=False)
    fig.update_yaxes(showgrid=False, zeroline=False)
    fig.update_layout(plot_bgcolor='#473081')
    fig.update_layout(hoverlabel_font_color='black', hoverlabel_bordercolor='white')
    
    #Axes
    fig.update_xaxes(title='x (au)', range=[-3.00,3.00], autorange=False)
    fig.update_yaxes(title='y (au)', range=[-3.00,3.00], autorange=False)
    
    #Plot all the orbits
    plot_orbit(xy_earth, '#3C65FE', fig)
    plot_orbit(xy_mars, '#DE422C', fig)
    plot_orbit(xy_venus, '#CD1D83', fig)
    plot_orbit(xy_mercury, '#1BA684', fig)
    plot_orbit(jupiter_half, '#DD7607', fig)
    
    #Plot all the planets                       
    plot_planet(x_s, y_s, 45, '#CF9046', 'Sun', fig)
    plot_planet(x_e, y_e, 20, '#3C65FE', 'Earth', fig)
    plot_planet(x_m, y_m, 14, '#DE422C', 'Mars', fig)
    plot_planet(x_v, y_v, 24, '#CD1D83', 'Venus', fig)
    plot_planet(x_me, y_me, 10, '#1BA684', 'Mercury', fig)
    plot_planet(x_j/2, y_j/2, 30, '#DD7607', 'Jupiter', fig)
    
    #Plot all the scpacecraft
    plot_spacecraft(x_bep, y_bep, 'BepiColombo', fig)
    plot_spacecraft(x_osi, y_osi, 'Osiris REx', fig)
    plot_spacecraft(x_mex, y_mex, 'Mars Express', fig)
    plot_spacecraft(x_psp, y_psp, 'Parker Solar Probe', fig)
    plot_spacecraft(x_sol, y_sol, 'Solar Orbiter', fig)
    plot_spacecraft(x_jaw, y_jaw, 'James Webb', fig)
    plot_spacecraft(x_jun/2, y_jun/2, 'Juno', fig)
    plot_spacecraft(x_jui, y_jui, 'Juice', fig)
    plot_spacecraft(x_haya, y_haya, 'Hayabusa2', fig)
    plot_spacecraft(x_aka, y_aka, 'Akatsuki', fig)
    
    #Spacecraft annotations
    annotate(x_bep-0.1, y_bep-0.15, 'BepiColombo', fig)
    annotate(x_osi+0.07, y_osi+0.2, 'Osiris REx', fig)
    annotate(x_mex+0.04, y_mex+0.15, 'Mars Express', fig)
    annotate(x_psp+0.33, y_psp+0.17, 'Parker Solar Probe', fig)
    annotate(x_sol+0.15, y_sol+0.15, 'Solar Orbiter', fig)
    annotate(x_jaw-0.15, y_jaw+0.15, 'James Webb', fig)
    annotate((x_jun/2)+0.1, (y_jun/2)+0.20, 'Juno', fig)
    annotate((x_jui)+0.1, (y_jui)+0.20, 'Juice', fig)
    annotate(x_haya, y_haya-0.15, 'Hayabusa2', fig)
    annotate(x_aka+0.32, y_aka, 'Akatsuki', fig)
    annotate(1.7, -2.7, 'Distance to Jupiter Not to Scale', fig)
    
    #Layout Details
    fig.update_layout(
        title = 'Positions of Space Craft Within the Solar System',
        title_font_family = 'Calibri',
        title_font_size = 20,
        font_family = 'Calibri',
        font_size = 15,
        title_font_color = 'black',
        autosize=False,
        width = 700,
        height = 700
        )
    fig.update_layout(showlegend=False)
    fig.update_layout(xaxis_fixedrange=True, yaxis_fixedrange=True)
    
    return fig

#Visibility Plot Information and Functions
#Date information
ts = load.timescale()
t = ts.now() + timedelta(days=1)

#Splitting the date out
d = str(t.utc_strftime().split(' ')[0])
today = ts.utc(int(d.split('-')[0]), int(d.split('-')[1]), int(d.split('-')[2]), 0)

today = ts.utc(int(d.split('-')[0]), int(d.split('-')[1]), int(d.split('-')[2]), 0)
date_correct1 = today.utc_strftime('%Y-%m-%d %H:%M:%S')
    
#The Next Day
t2 = t + 1
d2 = str(t2.utc_strftime().split(' ')[0])
tomorrow = ts.utc(int(d2.split('-')[0]), int(d2.split('-')[1]), int(d2.split('-')[2]), 0)
date_correct2 = tomorrow.utc_strftime('%Y-%m-%d %H:%M:%S')

#Times, 24 hour period each day
start = date_correct1
end = date_correct2


#Range of times for the plot
time_range = pd.date_range(start = start, end = end, freq = '10min')

#Space craft data functions
def ephem(name, station):
    data = Horizons(id=name, location=station, epochs={'start': start, 'stop': end, 'step': '10m'})
    ephem_data = data.ephemerides()
    return ephem_data

# Create dictionary for calculating angles
sc_codes = {
    'Mars Express':'-41',
    'BepiColombo':'-121',
    'Osiris REx':'-64',
    'Parker Solar':'-96',
    'Solar Orbiter':'-144',
    'James Webb':'-170',
    'Juno':'-61',
    'Juice':'-28',
    'Hayabusa2':'-37',
    'Akatsuki':'-5'}

def calculate_range(craft, t):
    code = sc_codes[craft]
    data = Horizons(id=code, location='500', epochs=t.jd)
    vec = data.vectors()
    Range = vec['range']
    return Range

def calculate_sot(craft, t):
    code = sc_codes[craft]
    data = Horizons(id=code, location='500', epochs=t.jd)
    eph = data.ephemerides()
    elong = eph['elong']
    return elong

#Plotting function
def plot_visibility(colour, y, name, fig):
    fig.add_traces(go.Scatter(mode = 'lines', 
                              line = go.scatter.Line(color=colour),
                              x = time_range, y = y,
                              name = '', 
                              hovertemplate =
                              '<b>'+name+'</b>'+
                              '<br>(%{x}, %{y}) deg'))

#Visibility Plot information
#Observatory Information 
hobart = '-75'
ceduna = '-74'
katherine = '-77'
yarragadee = '-78'
stations = [hobart, ceduna, katherine, yarragadee]

#Spacecraft information
bep = '-121'
osi = '-64'
mex = '-41'
psp = '-96'
sol = '-144'
jaw = '-170'
jun = '-61'
jui = '-28'
haya = '-37'
aka = '-5'
spacecraft = [bep, osi, mex, psp, sol, jaw, jun, jui, haya, aka]

#Space craft data functions
def epheme(name, station):
    data = Horizons(id=name, location=station, epochs={'start': start, 'stop': end, 'step': '10m'})
    ephem_data = data.ephemerides()
    return ephem_data

#Array for calling the horizons data
craft_data = [[0 for i in range(len(spacecraft))] for j in range(len(stations))] 

for i in range(len(stations)):
    for j in range(len(spacecraft)):
        craft_data[i][j] = epheme(spacecraft[j], stations[i])['EL']
        
#Plotting function
def plot_visability(colour, y, name, fig):
    fig.add_traces(go.Scatter(mode = 'lines', 
                              line = go.scatter.Line(color=colour),
                              x = time_range, y = y,
                              name = '', 
                              hovertemplate =
                              '<b>'+name+'</b>'+
                              '<br>(%{x}, %{y}) deg'))

#Functions for each station
def vis_hobart():
    fig = go.Figure()

    #Space Craft
    plot_visability('darkorange', craft_data[0][0], 'BepiColombo', fig)
    plot_visability('darkviolet', craft_data[0][1], 'Osiris REx', fig)
    plot_visability('red', craft_data[0][2], 'Mars Express', fig)
    plot_visability('blue', craft_data[0][3], 'Parker Solar Probe', fig)
    plot_visability('yellow', craft_data[0][4], 'Solar Orbiter', fig)
    plot_visability('deepskyblue', craft_data[0][5], 'James Webb', fig)
    plot_visability('brown', craft_data[0][6], 'Juno', fig)
    plot_visability('brown', craft_data[0][7], 'Juice', fig)
    plot_visability('green', craft_data[0][8], 'Hayabusa2', fig)
    plot_visability('hotpink', craft_data[0][9], 'Akatsuki', fig)

    #Plot Information
    fig.update_xaxes(title = 'Observing Time [UTC]')
    fig.update_yaxes(title = 'Elevation (deg)', range = [0,90])
    fig.update_layout(
        title = 'Elevation of Space Craft From Hobart',
        title_font_family = 'calibri',
        title_font_size = 20,
        font_family = 'calibri',
        font_size = 15,
        title_font_color = 'black',
        width = 750,
        height = 400
    )

    fig.update_layout(template = 'plotly_white')
    fig.update_layout(showlegend=False)
    return fig

#Ceduna
def vis_ceduna():
    fig = go.Figure()
    
    #Space Craft
    plot_visability('darkorange', craft_data[1][0], 'BepiColombo', fig)
    plot_visability('darkviolet', craft_data[1][1], 'Osiris REx', fig)
    plot_visability('red', craft_data[1][2], 'Mars Express', fig)
    plot_visability('blue', craft_data[1][3], 'Parker Solar Probe', fig)
    plot_visability('yellow', craft_data[1][4], 'Solar Orbiter', fig)
    plot_visability('deepskyblue', craft_data[1][5], 'James Webb', fig)
    plot_visability('brown', craft_data[1][6], 'Juno', fig)
    plot_visability('brown', craft_data[1][7], 'Juice', fig)
    plot_visability('green', craft_data[1][8], 'Hayabusa2', fig)
    plot_visability('hotpink', craft_data[1][9], 'Akatsuki', fig)

    #Plot Information
    fig.update_xaxes(title = 'Observing Time')
    fig.update_yaxes(title = 'Elevation (deg)', range = [0,90])
    fig.update_layout(
        title = 'Elevation of Space Craft From Ceduna',
        title_font_family = 'calibri',
        title_font_size = 20,
        font_family = 'calibri',
        font_size = 15,
        title_font_color = 'black',
        width = 750,
        height = 400
    )

    fig.update_layout(template = 'plotly_white')
    fig.update_layout(showlegend=False)
    return fig

#Katherine
def vis_katherine():
    fig = go.Figure()

    #Space Craft
    plot_visability('darkorange', craft_data[2][0], 'BepiColombo', fig)
    plot_visability('darkviolet', craft_data[2][1], 'Osiris REx', fig)
    plot_visability('red', craft_data[2][2], 'Mars Express', fig)
    plot_visability('blue', craft_data[2][3], 'Parker Solar Probe', fig)
    plot_visability('yellow', craft_data[2][4], 'Solar Orbiter', fig)
    plot_visability('deepskyblue', craft_data[2][5], 'James Webb', fig)
    plot_visability('brown', craft_data[2][6], 'Juno', fig)
    plot_visability('brown', craft_data[2][7], 'Juice', fig)
    plot_visability('green', craft_data[2][8], 'Hayabusa2', fig)
    plot_visability('hotpink', craft_data[2][9], 'Akatsuki', fig)

    #Plot Information
    fig.update_xaxes(title = 'Observing Time')
    fig.update_yaxes(title = 'Elevation (deg)', range = [0,90])
    fig.update_layout(
        title = 'Elevation of Space Craft From Katherine',
        title_font_family = 'calibri',
        title_font_size = 20,
        font_family = 'calibri',
        font_size = 15,
        title_font_color = 'black',
        width = 750,
        height = 400
    )

    fig.update_layout(template = 'plotly_white')
    fig.update_layout(showlegend=False)
    return fig

#Yarragadee
def vis_yarragadee():
    fig = go.Figure()

    #Space Craft
    plot_visability('darkorange', craft_data[3][0], 'BepiColombo', fig)
    plot_visability('darkviolet', craft_data[3][1], 'Osiris REx', fig)
    plot_visability('red', craft_data[3][2], 'Mars Express', fig)
    plot_visability('blue', craft_data[3][3], 'Parker Solar Probe', fig)
    plot_visability('yellow', craft_data[3][4], 'Solar Orbiter', fig)
    plot_visability('deepskyblue', craft_data[3][5], 'James Webb', fig)
    plot_visability('brown', craft_data[3][6], 'Juno', fig)
    plot_visability('brown', craft_data[3][7], 'Juice', fig)
    plot_visability('green', craft_data[3][8], 'Hayabusa2', fig)
    plot_visability('hotpink', craft_data[3][9], 'Akatsuki', fig)

    #Plot Information
    fig.update_xaxes(title = 'Observing Time')
    fig.update_yaxes(title = 'Elevation (deg)', range = [0,90])
    fig.update_layout(
        title = 'Elevation of Space Craft From Yarragadee',
        title_font_family = 'calibri',
        title_font_size = 20,
        font_family = 'calibri',
        font_size = 15,
        title_font_color = 'black',
        width = 750,
        height = 400
    )

    fig.update_layout(template = 'plotly_white')
    fig.update_layout(showlegend=False)
    return fig

#Define the plots
hobart = dcc.Graph(id='hobart', figure=vis_hobart())
ceduna = dcc.Graph(id='ceduna', figure=vis_ceduna())
katherine = dcc.Graph(id='katherine', figure=vis_katherine())
yarragadee = dcc.Graph(id='yarragadee', figure=vis_yarragadee())


Markdown = dcc.Markdown(id='Text', children='test')

station_dropdown = dcc.Dropdown(id='station_dropdown',
                                options=[
                                    {'label':'Hobart', 'value':'hobart'},
                                    {'label':'Ceduna', 'value':'ceduna'},
                                    {'label':'Katherine', 'value':'katherine'},
                                    {'label':'Yarragadee', 'value':'yarragadee'}],
                                style={'width':'50%', 'fontFamily':'Calibri'},
                                placeholder='Select a Station')

craft_dropdown = dcc.Dropdown(['Akatsuki', 'BepiColombo', 'Juno', 'Juice', 'Mars Express', 'Osiris REx', 'Parker Solar', 'James Webb', 'Hayabusa2', 'Solar Orbiter'],
                               id='craft_dropdown', style={'width':'50%', 'fontFamily':'Calibri'},
                               placeholder='Craft')

solar_map = dcc.Graph(id='solar_map',
                      figure=solar_system_map(), style={'display':'inline-block'})

#hobart_visibility = dcc.Graph(id='hobart_visibility',
                              #figure=visibility_plot(), style={'display':'inline-block', 'float':'right'})


app.layout = html.Div(children=[
    dcc.Interval(
            id='interval-component',
            interval=3600*1000, # in milliseconds
            n_intervals=0
        ),
    
    html.H1(children='Spacecraft Doppler Tracking', style={'fontFamily':'calibri'}),
    

    html.Div(children=[
        html.P('Dashboard for tracking and monitoring space craft targets for UTAS Space. \
            State vectors of the planets and craft are updated once per day. \
            The size of the planets, Sun and the orbit of Juno are not up to scale \
            for display purposes. The angle between the targets and visibility are correctly adjusted.'), 
        ], style={'width':'49%', 'display': 'block', 'fontFamily':'calibri'}),
    
    html.Div(children=[html.Label('Station'),
                       html.Br(), station_dropdown]),
    
    html.Br(),

    html.Div(children=[html.Label('Craft'),
                       html.Br(), craft_dropdown]),

    html.Br(),
    
    html.Div([
        html.Div(solar_map, style={'width' : '55%', 'height':'800px', 'display' : 'inline-block'}),
    
        html.Div([
            html.Div(children=[
                html.Div(id='graph', style={'display':'inline-block', 'float':'right'}),
                html.Br(),
                html.Div([
                    html.H2('Mission Selected:'),
                    html.Div(id='craft_dropdown-value'),
                    html.P('')
                ], style={'display' : 'inline-block', 'fontFamily':'Calibri'}),
                html.Br(),
                html.Div(id='distance', style={'color':'blue', 'fontFamily':'Calibri'}),
                html.Div(id='solar_elongation', style={'color':'blue', 'fontFamily':'Calibri'}),
                ])
        ], style={'width' : '40%', 'height':'800px', 'display' : 'inline-block', 'float': 'right'}),
    ]),
])

# Select the information menu
@app.callback(
    Output('craft_dropdown-value', 'children'),
    Output('distance', 'children'),
    Output('solar_elongation', 'children'),
    Input('craft_dropdown', 'value'))
def update_value(value):
    t = time.Time(start)
    if value:
        Range = calculate_range(value, t)
        sot = calculate_sot(value, t)
        return value, f'\n- Geocentric range: {Range.value[0]} AU', f'- Solar Elongation:{sot.value[0]} deg'
    else:
        return ['','','']

# Select the visibility plot
@app.callback(
    Output(component_id='graph', component_property='children'),
    Input(component_id='station_dropdown', component_property='value'))
def select_plot(value):
    if value == 'hobart':
        fig1 = hobart
        return fig1
    
    elif value == 'ceduna':
        fig2 = ceduna
        return fig2

    elif value == 'katherine':
        fig3 = katherine
        return fig3

    elif value == 'yarragadee':
        fig4 = yarragadee
        return fig4
    else:
        return None

# Update all the plots
#@app.callback(Output('live-update-graph', 'figure'),
#              Input('interval-component', 'n_intervals'))
#def update_solar_map():
#    solar_system_map()

if __name__ == '__main__':
    app.run_server(debug=False)
