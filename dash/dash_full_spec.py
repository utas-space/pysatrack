import glob
import os
import pathlib

import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import plotly.express as px
import re

import dash
from dash.dependencies import Input, Output, State

APP_DATA = '/mnt/data/spectra/hb/'
app = dash.Dash(__name__, routes_pathname_prefix='/dash/')

colors = {
    'background': '#FFFFFF',
    'text_title': '#7FDBFF'
}

# Generate the spectral plot with full bandwidth
def generate_plot(antenna, freqs, n_spec):
    APP_DATA = f'/mnt/data/spectra/{antenna}/'
    file_name: str = glob.glob(f'{APP_DATA}*.bin')[0]
    try:
        fd = open(file_name, 'rb')
    except:
        print(f'Error file not found')

    fsize: int = os.path.getsize(file_name) / 4

    match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:N)?([oabcdefgh])?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_([^_]*)", file_name, re.I)

    FFTpt = int(match.group(6))
    Nfft: int = int(FFTpt/2 + 1)
    bandwidth: float = 32e6

    fmin: float = freqs[0]
    fmax: float = freqs[1]

    df: int = int(bandwidth / Nfft)
    ff: float = np.arange(Nfft) * df
    bmin: int = int(fmin / df)
    bmax: int = int(fmax / df)

    Nspec: int = int(fsize / Nfft - 1)

    print(f'it should read: {n_spec}')
    one_spec: float = np.fromfile(file=fd, dtype="float32", count=Nfft, offset=int(n_spec) * Nfft)#offset=Nfft * 4 * Nspec
    one_spec /= one_spec.mean()
    
    zoom_spec = one_spec[bmin : bmax]
    zoom_ff = ff[bmin : bmax]

    #fig = px.line(x=ff, y=10*np.log10(one_spec), color_discrete_sequence=px.colors.qualitative.Vivid)
    fig = px.line(x=zoom_ff, y=10*np.log10(zoom_spec), color_discrete_sequence=px.colors.qualitative.Vivid)
    fig.update_layout(
        title_text=f"<b>Dashboard with the full spectra - {int(bandwidth / 1e6)} MHz</b>",
        width=1500,
        height=700,
        font_family="lato",
        font_size=19,
    ).update_xaxes(title='<b>Full spectra in the frequency band [Hz]</b>'
    ).update_yaxes(title_text="<b>Relative Power (dB)</b>")
    fd.close()

    return fig

fig = generate_plot(antenna='hb', freqs=[0, 32e6], n_spec=0)

app.layout = html.Div(style={'backgroundColor': colors['background']},
    children = [
        html.H1(id='title-panel', 
            children='Satellite Tracking Dashboard',
            style={'textAlign': 'center', 'color': colors['text_title']}),

        html.Div([dcc.Dropdown(id='antenna-dropdown',
            options=[
                {'label' : 'Hobart-12', 'value': 'hb'},
                {'label' : 'Katherine', 'value' : 'ke'},
                {'label' : 'Yarragadee', 'value' : 'yg'}]
            ),html.Div(id='antenna-container', style={'textAlign': 'center'})]),
        
        html.Br(),
        html.Label('Number of spectra'),
        dcc.Input(id='spectra-value', value=0, type='text'),

        html.Div(id="main-page",
            children = [
                dcc.Interval(id='interval', interval=10*1000, n_intervals=0),
                dcc.Graph(id='spectrum', figure=fig),
            ]),

        html.Div([dcc.RangeSlider(
            id='freq-slider',
            min=0,
            max=32e6,
            step=100,
            tooltip={"placement": "bottom", "always_visible": True},
            value=[0, 32e6]
            ),html.Div(id='slider-output-container')
        ])
    ]
)

@app.callback(
    Output("spectrum", "figure"),
    [Input("interval", "n_intervals"),
    Input('antenna-dropdown', 'value'),
    Input('freq-slider', 'value'),
    Input('spectra-value', 'value')]
)
def update_plot(fig, antenna, freqs, nspec):
    return generate_plot(antenna, freqs, nspec)

if __name__ == '__main__':
    app.run_server(debug=True)
